module cpu_tb;
	reg clock;
	
	CPU cpu1 (clock);// Instantiate CPU module  
	
	initial
	begin
		$dumpfile("CPU.vcd");
		$dumpvars(0, cpu_tb);
	end
	
	initial 
	begin
		clock = 0;
		repeat (200)
		begin
			#10 clock = ~clock; //alternate clock signal
		end
	end
			
	initial #2000 $finish; //end simulation after 100 periods
endmodule
