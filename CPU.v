//Author name: Michael Moore & Marc Patton
//Last Modified Date: 11/18/2015
//Compilation Status: Successful
//Elaboration Status: Successful
//Simulation Status: Simulates Correctly


module CPU (clock);
	
	//Opcodes for supported instructions
	parameter lw	=	6'b100011;
	parameter sw	=	6'b101011;
	
	parameter add	=	6'b000000; //For any R type arithmetic instruction
	parameter addi	=	6'b001000;
	parameter andi	=	6'b001100;
	parameter ori	=	6'b001101;
	parameter nori	=	6'b001111;
	parameter oprm	=	6'b001110; //Op code for all immediate R type operations is the same
	
	parameter beq	=	6'b000100;
	parameter bne	=	6'b000101;
	parameter j		=	6'b000010;
	
	input clock;  //Clock Input
	// The architecturally visible Regsisters and scratch Registers for implementation
	
	reg[31:0] Regs[0:31], Memory[0:1023], PC, IR, ALUOut, MDR, A, B;
	reg[2:0] state; // processor state
	
	
	wire [5:0] funccode, opcode; //use to get opcode easily
	wire [4:0] rs, rt, rd;
	wire [31:0] SignExtend, PCOffset, zero, s0, s1, s2, s3, t1, rm; //used to get sign extended offset field
	
	assign opcode = IR[31:26];
	assign funccode = IR[5:0]; //need function code for R type operations
	assign rs = IR[25:21];
	assign rt = IR[20:16];
	assign rd = IR[15:11];
	assign SignExtend = {{16{IR[15]}},IR[15:0]}; //sign extension of lower 16-bits of instruction
	assign PCOffset = SignExtend; //PC offset is shifted
	
	assign zero = Regs[0];
	assign s0 = Regs[16];
	assign s1 = Regs[17];
	assign t1 = Regs[9];
	assign s2 = Regs[18];
	assign s3 = Regs[19];
	assign rm = Memory[240];
	
		//Constants for the function field
	parameter func_add	=	6'b100000; //20
	parameter func_and	= 	6'b100100; //24
	parameter func_sub	=	6'b100010; //22
	parameter func_or	=	6'b100101; //25
	parameter func_nor	=	6'b100111; //27
	
	// set the PC to 0 and start the control in state 1 to start fetch instructions from Memory[0] (byte 8)
	initial 
	begin 

		//Load program into memory
		Memory[0] = 32'h02114820;	//add $t1, $s0, $s1 0000 0010 0001 0001 0100 1000 <-- after execution $t1 equals 22
		Memory[1] = 32'h02114822;	//sub $t1, $s0, $s1 <-- after execution $t1 equals 16
		Memory[2] = 32'h02114824;	//and $t1, $s0, $s1 <-- after execution $t1 equals 03
		Memory[3] = 32'h02114825;	//or $t1, $s0, $s1 <-- after execution $t1 equals 13
		Memory[4] = 32'h02114827;	//nor $t1, $s0, $s1 <-- after execution $t1 equals 0x2C
		Memory[5] = 32'h08000009;	//j 9 <-- jumps to next instruction
		Memory[15] = 32'h2209000E;	//addi $t1, $s0, 14 0010 0010 0000 1001 0000 0000
		Memory[16] = 32'h3209000E;	//andi $t1, $s0, 14
		Memory[17] = 32'h3609000E;	//ori $t1, $s0, 14
		Memory[18] = 32'h3E09000E;	//nori $t1, $s0, 14
		Memory[19] = 32'h1611000A; //bne $s0, $s1, 10
		Memory[30] = 32'h3A124820; //addrm $t1, $s0, $s2
		Memory[31] = 32'h3A124822; //subrm $t1, $s0, $s2
		Memory[32] = 32'h3A124825; //orrm $t1, $s0, $s2
		Memory[33] = 32'h3A124824; //andrm $t1, $s0, $s2
		Memory[34] = 32'h8E500000; //lw $s0, 0($s2) 1000 1110 0101 0000
		Memory[35] = 32'h8E510000; //lw $s1, 0($s2) 1000   1110 0101 0001
		Memory[36] = 32'hAE600000; //sw $zero, 0($s3) 1010 1110 0110 0000
		Memory[37] = 32'h1211FFDB; //beq $s0, $s1 -37  0001 0010 0001 0001 <-- Back to the first instruction, loop
		
		Regs[16] <= 8'h13;
		Regs[17] <= 4'h3;
		Regs[18] <= 8'hF0;
		Regs[19] <= 8'hF0;
		Memory[240] <= 4'hE;
		
		PC = 0;
		state = 1; 
	end
	always @(posedge clock) begin
		
	//short-cut way to make sure R0 is always 0
	Regs[0] = 0;  //make R0 0
	
	case (state) //action depends on the state
	
		//first step: fetch the instruction, increment PC, go to next state	
		1: begin     
			IR <= Memory[PC[31:0]];
			PC <= PC + 1;
			state = 2; //next state
			end
		
		//second step: Instruction decode, Register fetch, also compute branch address
		2: begin     
			A <= Regs[rs];
			state= 3;
			ALUOut <= PC + PCOffset; 	// compute PC-relative branch target
			
			//Add conditions for R type immediate and R type register memory
			//For Immediate instructions 
			if ((opcode==addi) || (opcode==andi) || (opcode==ori) || (opcode==nori)) begin
				B <= SignExtend;
				end	
				
			else if (opcode==oprm) begin
				ALUOut <= Regs[rt];
				state=3;
				end
				
			else
				B <= Regs[rt];
			end
		
		//Third step: Instruction Dependent:
		3: begin
			state = 4; // default next state
			if ((opcode==lw) | (opcode==sw)) 
				ALUOut <= A + SignExtend; //compute effective address
			
			else if (opcode == add) begin
				case (funccode) //case for the various R-type instructions
					func_add: ALUOut = A + B; //ADD Operation
					func_sub: ALUOut = A - B;
					func_and: ALUOut = A & B; //Bitwise AND Operation
					func_or: ALUOut = A | B; //Bitwise OR Operation
					func_nor: ALUOut = !(A | B); //NOR Operation
				endcase
				end
				
			else if((opcode==addi) || (opcode==andi) || (opcode==ori) || (opcode==nori)) begin
				case(opcode)
					addi: ALUOut = A + B; //ADD Operation
					andi: ALUOut = A & B; //Bitwise AND Operation
					ori: ALUOut = A | B; //Bitwise OR Operation
					nori: ALUOut = !(A | B); //NOR Operation			
				endcase
				end
			
			//Register Memory instructions
			else if (opcode==oprm) begin
				B <= Memory[ALUOut];
				state = 4;
				end
				
			else if (opcode == bne) begin//BNE
				if(A!=B) PC <= ALUOut;
				state = 1;
				end
					
			else if (opcode == beq) begin //BEQ
				if(A==B) PC <= ALUOut;
				state = 1;
				end
				
			else if(opcode==j) begin//jump
				PC = PC + IR[25:0];
				state = 1;
				end
			end
			
		4: begin
			if (opcode == add) begin
				Regs[rd] <= ALUOut; // write the result
				state = 1;
				end //R-type finishes
				
			else if ((opcode==addi) || (opcode==andi) || (opcode==ori) || (opcode==nori)) begin
				Regs[rt] <= ALUOut;
				state=1;
				end
				
			else if (opcode==oprm) begin
				case (funccode) //case for the various R-type instructions
					func_add: ALUOut = A + B; //ADD Operation
					func_sub: ALUOut = A - B; //SUB Operation  
					func_and: ALUOut = A & B; //Bitwise AND Operation
					func_or: ALUOut = A | B; //Bitwise OR Operation
				endcase
				state = 5;
				end //R-type finishes
		
			else if (opcode==lw) begin
				MDR <= Memory[ALUOut]; // read the memory
				state = 5; // next state
				end

			else if (opcode==sw) begin
				Memory[ALUOut] <= B; // write the memory
				state = 1; // return to state 1
				end //store finishes
			end
			
		5: 	begin 
				state = 1;
				if (opcode==lw) begin
					Regs[rt] <= MDR;	// write the MDR to the Register
					end
				else
					Regs[rd] <= ALUOut; //RM instructions
			end
	endcase
	end	
endmodule

module cpu_tb;
	reg clock;
	
	CPU cpu1 (clock);// Instantiate CPU module  
	
	initial 
	begin
		clock = 0;
		repeat (200)
		begin
			#10 clock = ~clock; //alternate clock signal
		end
	end
			
	initial #2000 $finish; //end simulation after 100 periods
endmodule
