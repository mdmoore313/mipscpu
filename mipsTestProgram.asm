#Starting Memory Values
#Memory[30] = 0x0001	location 120($0)	
#Memory[31] = 0x0010	location 124($0)
#Memory[31] = 0x0000	location 128($0)
#Memory[32] = 0x1111    location 132($0)

	lw $t0, 120($0)	#test lw completed
	#sw 136($0), $t0 #test sw completed
	
	lw $t1, 120($0) #case for equal
	
	beq $t1, $t0, equal  
	bne $t1, $t0, notEqual	

loop:	lw $t1, 124($0) #case for not equal
	
	beq $t1, $t0, equal  #test beq complete
	bne $t1, $t0, notEqual  #test bne complete

done:	add $v0, $t1, $t0  #test add complete 
	sub $v0, $t1, $t0  #test sub complete
	addi $v0, $t1, 5

	#pick matching pairs for test cases

	#Case: and = 0, or = 1, nor = 0
	lw $t2, 128($0) #load 32 bit low 
	lw $t3, 132($0) #load 32 bit high
	
	and $v0, $t3, $t2
	or $v0, $t3, $t2
	nor $v0, $t3, $t2

	#Case: and = 1, or = 1, nor = 0
	lw $t2, 132($0) #load 32 bit high 
	lw $t3, 132($0) #load 32 bit high

	and $v0, $t3, $t2  #test and complete
	or $v0, $t3, $t2  #test or complete 	
	nor $v0, $t3, $t2  #test nor complete


	#Case: imm==1
	#andi = 0, ori = 1, nori = 0
	#Case: imm==0
	#andi = 1, ori = 0, nori = 1
 	lw $t3, 132($0) #load 32 bit low  

	andi $v0, $t3, 0x1111
	ori $v0, $t3, 0x1111
	#nori $v0, $t3, 0x1111
	andi $v0, $t3, 0x0000 
	ori $v0, $t3, 0x0000  
	#nori $v0, $t3, 0x0000 

	#Case: imm==1
	#andi = 1, ori = 1, nori = 0
	#Case: imm==0
	#andi = 0, ori = 1, nori = 0
	#Case: andi = 1, ori = 1, nori = 0
	lw $t3, 132($0) #load 32 bit high

	andi $v0, $t3, 0x1111
	ori $v0, $t3, 0x1111
	#nori $v0, $t3, 0x1111
	andi $v0, $t3, 0x0000  #test andi complete
	ori $v0, $t3, 0x0000  #test ori complete
	#nori $v0, $t3, 0x0000  #test nori complete

	lw $t3, 132($0) #load 32 bit high

	addrm $t3, 100000, 120($0) 
	subrm $t3, 100010, 120($0)


	#Case: memory== high
	#andrm = 1, orrm = 1
	#Case: memory==low
	#andrm = 0, orrm = 1
	lw $t3 128($0) #load 32 bit low

	orrm $t3, or, 128($0)
	andrm $t3, and, 128($0)
	orrm $t3, or, 132($0)
	andrm $t3, and, 132($0)


	#Case: memory== high
	#andrm = 1, orrm = 1
	#Case: memory==low
	#andrm = 0, orrm = 1
	lw $t3, 132($0) #load 32 bit high

	orrm $t3, or, 128($0)
	andrm $t3, and, 128($0)
	orrm $t3, or, 132($0)
	andrm $t3, and, 132($0)

equal:
	j loop #test jump complete
notEqual:
	j done
